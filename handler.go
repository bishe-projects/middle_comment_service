package main

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/middle_comment_service/src/interface/facade"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/comment"
)

// CommentServiceImpl implements the last service interface defined in the IDL.
type CommentServiceImpl struct{}

// CreateCommentEntity implements the CommentServiceImpl interface.
func (s *CommentServiceImpl) CreateCommentEntity(ctx context.Context, req *comment.CreateCommentEntityReq) (resp *comment.CreateCommentEntityResp, err error) {
	klog.CtxInfof(ctx, "CreateCommentEntity req=%+v", req)
	resp = facade.CommentEntityFacade.Create(ctx, req)
	klog.CtxInfof(ctx, "CreateCommentEntity resp=%+v", resp)
	return
}

// AllCommentEntityList implements the CommentServiceImpl interface.
func (s *CommentServiceImpl) AllCommentEntityList(ctx context.Context, req *comment.AllCommentEntityListReq) (resp *comment.AllCommentEntityListResp, err error) {
	klog.CtxInfof(ctx, "AllCommentEntityList req=%+v", req)
	resp = facade.CommentEntityFacade.All(ctx, req)
	klog.CtxInfof(ctx, "AllCommentEntityList resp=%+v", resp)
	return
}

// PublishComment implements the CommentServiceImpl interface.
func (s *CommentServiceImpl) PublishComment(ctx context.Context, req *comment.PublishCommentReq) (resp *comment.PublishCommentResp, err error) {
	klog.CtxInfof(ctx, "PublishComment req=%+v", req)
	resp = facade.CommentFacade.Publish(ctx, req)
	klog.CtxInfof(ctx, "PublishComment resp=%+v", resp)
	return
}

// DeleteComment implements the CommentServiceImpl interface.
func (s *CommentServiceImpl) DeleteComment(ctx context.Context, req *comment.DeleteCommentReq) (resp *comment.DeleteCommentResp, err error) {
	klog.CtxInfof(ctx, "DeleteComment req=%+v", req)
	resp = facade.CommentFacade.Delete(ctx, req)
	klog.CtxInfof(ctx, "DeleteComment resp=%+v", resp)
	return
}

// EntityCommentList implements the CommentServiceImpl interface.
func (s *CommentServiceImpl) EntityCommentList(ctx context.Context, req *comment.EntityCommentListReq) (resp *comment.EntityCommentListResp, err error) {
	klog.CtxInfof(ctx, "EntityCommentList req=%+v", req)
	resp = facade.CommentFacade.EntityCommentList(ctx, req)
	klog.CtxInfof(ctx, "EntityCommentList resp=%+v", resp)
	return
}

// UserCommentList implements the CommentServiceImpl interface.
func (s *CommentServiceImpl) UserCommentList(ctx context.Context, req *comment.UserCommentListReq) (resp *comment.UserCommentListResp, err error) {
	klog.CtxInfof(ctx, "UserCommentList req=%+v", req)
	resp = facade.CommentFacade.UserCommentList(ctx, req)
	klog.CtxInfof(ctx, "UserCommentList resp=%+v", resp)
	return
}

// GetCommentById implements the CommentServiceImpl interface.
func (s *CommentServiceImpl) GetCommentById(ctx context.Context, req *comment.GetCommentByIdReq) (resp *comment.GetCommentByIdResp, err error) {
	klog.CtxInfof(ctx, "GetCommentById req=%+v", req)
	resp = facade.CommentFacade.GetCommentByID(ctx, req)
	klog.CtxInfof(ctx, "GetCommentById resp=%+v", resp)
	return
}
