package main

import (
	"github.com/cloudwego/kitex/server"
	comment "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/comment/commentservice"
	"log"
	"net"
)

func main() {
	addr, _ := net.ResolveTCPAddr("tcp", "127.0.0.1:8887")
	svr := comment.NewServer(new(CommentServiceImpl), server.WithServiceAddr(addr))

	err := svr.Run()

	if err != nil {
		log.Println(err.Error())
	}
}
