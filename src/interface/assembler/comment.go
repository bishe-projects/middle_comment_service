package assembler

import (
	"gitlab.com/bishe-projects/middle_comment_service/src/domain/comment/entity"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/comment"
)

func ConvertPublishCommentReqToEntity(req *comment.PublishCommentReq) *entity.Comment {
	return &entity.Comment{
		Uid:          req.Uid,
		BizId:        req.BizId,
		EntityTypeId: req.EntityTypeId,
		EntityId:     req.EntityId,
		Content:      req.Content,
		ParentId:     req.ParentId,
	}
}

func ConvertDeleteCommentReqToEntity(req *comment.DeleteCommentReq) *entity.Comment {
	return &entity.Comment{
		ID:    req.Id,
		BizId: req.BizId,
	}
}

func ConvertEntityToComment(entity *entity.Comment) *comment.Comment {
	return &comment.Comment{
		Id:           entity.ID,
		BizId:        entity.BizId,
		EntityTypeId: entity.EntityTypeId,
		EntityId:     entity.EntityId,
		Uid:          entity.Uid,
		ParentId:     entity.ParentId,
		Content:      entity.Content,
		CreateTime:   entity.CreateTime.Format("2006-01-02 03:04:05"),
		ReplyCount:   entity.ReplyCount,
	}
}

func ConvertEntityListToCommentList(entityList []*entity.Comment) []*comment.Comment {
	commentList := make([]*comment.Comment, 0, len(entityList))
	for _, e := range entityList {
		commentList = append(commentList, ConvertEntityToComment(e))
	}
	return commentList
}
