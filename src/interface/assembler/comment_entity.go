package assembler

import (
	"gitlab.com/bishe-projects/middle_comment_service/src/domain/comment_entity/entity"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/comment"
)

func ConvertCreateCommentEntityReqToEntity(req *comment.CreateCommentEntityReq) *entity.CommentEntity {
	return &entity.CommentEntity{
		BizId: req.BizId,
		Name:  req.Name,
		Desc:  req.Desc,
	}
}

func ConvertEntityToCommentEntity(entity *entity.CommentEntity) *comment.CommentEntity {
	return &comment.CommentEntity{
		Id:    entity.ID,
		BizId: entity.BizId,
		Name:  entity.Name,
		Desc:  entity.Desc,
	}
}

func ConvertEntityListToCommentEntityList(entityList []*entity.CommentEntity) []*comment.CommentEntity {
	commentEntityList := make([]*comment.CommentEntity, 0, len(entityList))
	for _, e := range entityList {
		commentEntityList = append(commentEntityList, ConvertEntityToCommentEntity(e))
	}
	return commentEntityList
}
