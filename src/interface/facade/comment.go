package facade

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_comment_service/src/application/service"
	"gitlab.com/bishe-projects/middle_comment_service/src/interface/assembler"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/comment"
)

var CommentFacade = new(Comment)

type Comment struct{}

func (f *Comment) Publish(ctx context.Context, req *comment.PublishCommentReq) *comment.PublishCommentResp {
	resp := comment.NewPublishCommentResp()
	entity := assembler.ConvertPublishCommentReqToEntity(req)
	err := service.CommentApp.Publish(entity)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommentFacade] publish comment failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.Comment = assembler.ConvertEntityToComment(entity)
	return resp
}

func (f *Comment) Delete(ctx context.Context, req *comment.DeleteCommentReq) *comment.DeleteCommentResp {
	resp := comment.NewDeleteCommentResp()
	entity := assembler.ConvertDeleteCommentReqToEntity(req)
	err := service.CommentApp.Delete(entity)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommentFacade] delete comment failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
	}
	return resp
}

func (f *Comment) GetCommentByID(ctx context.Context, req *comment.GetCommentByIdReq) *comment.GetCommentByIdResp {
	resp := comment.NewGetCommentByIdResp()
	entity, err := service.CommentApp.GetByID(req.Id, req.BizId)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommentFacade] get comment by id failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	if entity != nil {
		resp.Comment = assembler.ConvertEntityToComment(entity)
	}
	return resp
}

func (f *Comment) EntityCommentList(ctx context.Context, req *comment.EntityCommentListReq) *comment.EntityCommentListResp {
	resp := comment.NewEntityCommentListResp()
	commentList, count, err := service.CommentApp.EntityCommentList(req.BizId, req.EntityTypeId, req.EntityId, req.ParentId, req.PageNum, req.PageSize)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommentFacade] get entity comment list failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.CommentList = assembler.ConvertEntityListToCommentList(commentList)
	resp.Total = count
	return resp
}

func (f *Comment) UserCommentList(ctx context.Context, req *comment.UserCommentListReq) *comment.UserCommentListResp {
	resp := comment.NewUserCommentListResp()
	commentList, count, err := service.CommentApp.UserCommentList(req.BizId, req.EntityTypeId, req.Uid, req.PageNum, req.PageSize)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommentFacade] get user comment list failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.CommentList = assembler.ConvertEntityListToCommentList(commentList)
	resp.Total = count
	return resp
}
