package facade

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_comment_service/src/application/service"
	"gitlab.com/bishe-projects/middle_comment_service/src/interface/assembler"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/comment"
)

var CommentEntityFacade = new(CommentEntity)

type CommentEntity struct{}

func (f *CommentEntity) Create(ctx context.Context, req *comment.CreateCommentEntityReq) *comment.CreateCommentEntityResp {
	resp := comment.NewCreateCommentEntityResp()
	entity := assembler.ConvertCreateCommentEntityReqToEntity(req)
	err := service.CommentEntityApp.Create(entity)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommentEntityFacade] create comment entity failed: req=%+v err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.CommentEntity = assembler.ConvertEntityToCommentEntity(entity)
	return resp
}

func (f *CommentEntity) All(ctx context.Context, req *comment.AllCommentEntityListReq) *comment.AllCommentEntityListResp {
	resp := comment.NewAllCommentEntityListResp()
	entityList, err := service.CommentEntityApp.All(req.BizId)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommentEntityFacade] get all comment entity list failed: req=%+v err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.CommentEntityList = assembler.ConvertEntityListToCommentEntityList(entityList)
	return resp
}
