package po

import "time"

type Comment struct {
	ID           int64
	Uid          int64
	BizId        int64
	EntityTypeId int64
	EntityId     int64
	Content      string `gorm:"type:text"`
	ParentId     int64
	CreateTime   time.Time `gorm:"default:null"`
	UpdateTime   time.Time `gorm:"default:null"`
	IsDelete     int8
}

type CommentResult struct {
	ID           int64
	Uid          int64
	BizId        int64
	EntityTypeId int64
	EntityId     int64
	ParentId     int64
	Content      string    `gorm:"type:text"`
	CreateTime   time.Time `gorm:"default:null"`
	ReplyCount   int64
}
