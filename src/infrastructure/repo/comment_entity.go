package repo

import (
	"gitlab.com/bishe-projects/middle_comment_service/src/infrastructure/repo/mysql"
	"gitlab.com/bishe-projects/middle_comment_service/src/infrastructure/repo/po"
)

type CommentEntityDao interface {
	Create(entity *po.CommentEntity) error
	All(bizId *int64) ([]*po.CommentEntity, error)
}

var CommentEntityRepo = NewCommentEntity(mysql.CommentEntityDao)

type CommentEntity struct {
	CommentEntityDao CommentEntityDao
}

func NewCommentEntity(commentEntityDao CommentEntityDao) *CommentEntity {
	return &CommentEntity{
		CommentEntityDao: commentEntityDao,
	}
}
