package mysql

import (
	"github.com/apache/thrift/lib/go/thrift"
	"gitlab.com/bishe-projects/middle_comment_service/src/infrastructure/repo/po"
	"reflect"
	"testing"
)

func TestComment_Create(t *testing.T) {
	type args struct {
		comment *po.Comment
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "normal",
			args: args{
				comment: &po.Comment{
					Uid:          1,
					BizId:        1,
					EntityTypeId: 1,
					EntityId:     1,
					Content:      "test2",
					ParentId:     0,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Comment{}
			if err := r.Create(tt.args.comment); (err != nil) != tt.wantErr {
				t.Errorf("Create() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestComment_GetList(t *testing.T) {
	type args struct {
		bizId        int64
		entityTypeId int64
		uid          *int64
		entityId     *int64
		parentId     *int64
		pageNum      *int64
		pageSize     *int64
	}
	tests := []struct {
		name    string
		args    args
		want    []*po.CommentResult
		wantErr bool
	}{
		{
			name: "normal",
			args: args{
				bizId:        1,
				entityTypeId: 1,
				uid:          thrift.Int64Ptr(1),
				entityId:     thrift.Int64Ptr(1),
				parentId:     thrift.Int64Ptr(0),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Comment{}
			got, err := r.GetList(tt.args.bizId, tt.args.entityTypeId, tt.args.uid, tt.args.entityId, tt.args.parentId, tt.args.pageNum, tt.args.pageSize)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetList() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetList() got = %v, want %v", got, tt.want)
			}
			for _, comment := range got {
				t.Logf("comment=%+v", comment)
			}
		})
	}
}

func TestComment_Count(t *testing.T) {
	type args struct {
		bizId        int64
		entityTypeId int64
		uid          *int64
		entityId     *int64
		parentId     *int64
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "normal",
			args: args{
				bizId:        4,
				entityTypeId: 1,
				uid:          thrift.Int64Ptr(1),
				entityId:     nil,
				parentId:     nil,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Comment{}
			got, err := r.Count(tt.args.bizId, tt.args.entityTypeId, tt.args.uid, tt.args.entityId, tt.args.parentId)
			t.Logf("count=%d", got)
			if (err != nil) != tt.wantErr {
				t.Errorf("Count() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
