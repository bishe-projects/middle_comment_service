package mysql

import (
	"gitlab.com/bishe-projects/common_utils/error_enum"
	"gitlab.com/bishe-projects/common_utils/mysql"
	"gitlab.com/bishe-projects/middle_comment_service/src/infrastructure/repo/po"
)

var CommentDao = new(Comment)

type Comment struct{}

const commentTable = "comment"

func (r *Comment) Create(comment *po.Comment) error {
	err := db.Table(commentTable).Create(&comment).Error
	if err != nil {
		if code := mysql.MySqlErrCode(err); code == mysql.ErrDuplicateEntryCode {
			return error_enum.ErrDuplicateEntry
		}
		return err
	}
	return nil
}

func (r *Comment) Delete(id, bizId int64) error {
	return db.Table(commentTable).Where("id = ? and biz_id = ?", id, bizId).Update("is_delete", 1).Error
}

func (r *Comment) GetByID(id, bizId int64) (*po.CommentResult, error) {
	var comment *po.CommentResult
	result := db.Table(commentTable).Where("id = ? and biz_id = ?", id, bizId).First(&comment)
	return comment, result.Error
}

func (r *Comment) Count(bizId, entityTypeId int64, uid, entityId, parentId *int64) (int64, error) {
	var count int64
	d := db.Table(commentTable).Where("biz_id = ? and entity_type_id = ? and is_delete = ?", bizId, entityTypeId, 0)
	if uid != nil {
		d = d.Where("uid = ?", *uid)
	}
	if entityId != nil {
		d = d.Where("entity_id = ?", *entityId)
	}
	if parentId != nil {
		d = d.Where("parent_id = ?", *parentId)
	}
	result := d.Count(&count)
	return count, result.Error
}

func (r *Comment) GetList(bizId, entityTypeId int64, uid, entityId, parentId, pageNum, pageSize *int64) ([]*po.CommentResult, error) {
	var commentList []*po.CommentResult
	subQuery := db.Table("comment c2").Select("COUNT(*)").Where("c1.id = c2.parent_id")
	d := db.Table("comment c1").Select("c1.id, c1.uid, c1.biz_id, c1.entity_type_id, c1.entity_id, c1.parent_id, c1.content, c1.create_time, (?) as reply_count", subQuery).Where("c1.biz_id = ? and c1.entity_type_id = ? and c1.is_delete = ?", bizId, entityTypeId, 0)
	if uid != nil {
		d = d.Where("c1.uid = ?", *uid)
	}
	if entityId != nil {
		d = d.Where("c1.entity_id = ?", *entityId)
	}
	if parentId != nil {
		d = d.Where("c1.parent_id = ?", *parentId)
	}
	d = d.Order("c1.create_time")
	if pageNum != nil && pageSize != nil {
		d.Offset(int((*pageNum - 1) * *pageSize)).Limit(int(*pageSize))
	}
	result := d.Scan(&commentList)
	return commentList, result.Error
}
