package mysql

import (
	"gitlab.com/bishe-projects/common_utils/error_enum"
	"gitlab.com/bishe-projects/common_utils/mysql"
	"gitlab.com/bishe-projects/middle_comment_service/src/infrastructure/repo/po"
)

var CommentEntityDao = new(CommentEntity)

type CommentEntity struct{}

const commentEntityTable = "comment_entity"

func (r *CommentEntity) Create(entity *po.CommentEntity) error {
	err := db.Table(commentEntityTable).Create(&entity).Error
	if err != nil {
		if code := mysql.MySqlErrCode(err); code == mysql.ErrDuplicateEntryCode {
			return error_enum.ErrDuplicateEntry
		}
		return err
	}
	return nil
}

func (r *CommentEntity) All(bizId *int64) ([]*po.CommentEntity, error) {
	var entityList []*po.CommentEntity
	d := db.Table(commentEntityTable)
	if bizId != nil {
		d = d.Where("biz_id = ?", *bizId)
	}
	result := d.Scan(&entityList)
	return entityList, result.Error
}
