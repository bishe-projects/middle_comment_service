package repo

import (
	"gitlab.com/bishe-projects/middle_comment_service/src/infrastructure/repo/mysql"
	"gitlab.com/bishe-projects/middle_comment_service/src/infrastructure/repo/po"
)

type CommentDao interface {
	Create(comment *po.Comment) error
	Delete(id, bizId int64) error
	GetByID(id, bizId int64) (*po.CommentResult, error)
	Count(bizId, entityTypeId int64, uid, entityId, parentId *int64) (int64, error)
	GetList(bizId, entityTypeId int64, uid, entityId, parentId, pageNum, pageSize *int64) ([]*po.CommentResult, error)
}

var CommentRepo = NewComment(mysql.CommentDao)

type Comment struct {
	CommentDao CommentDao
}

func NewComment(commentDao CommentDao) *Comment {
	return &Comment{
		CommentDao: commentDao,
	}
}
