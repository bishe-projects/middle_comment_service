package biz_error

import "gitlab.com/bishe-projects/common_utils/business_error"

var (
	CreateCommentEntityErr  = business_error.NewBusinessError("create commenet entity failed", -10001)
	AllCommentEntityListErr = business_error.NewBusinessError("get all comment entity list failed", -10002)
	PublishCommentErr       = business_error.NewBusinessError("publish comment failed", -10003)
	DeleteCommentErr        = business_error.NewBusinessError("delete comment failed", -10004)
	GetCommentListCountErr  = business_error.NewBusinessError("get comment list count failed", -10005)
	GetCommentListErr       = business_error.NewBusinessError("get comment list failed", -10006)
	GetCommentByIDErr       = business_error.NewBusinessError("get comment by id failed", -10007)
)
