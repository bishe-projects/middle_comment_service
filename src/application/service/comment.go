package service

import (
	"github.com/apache/thrift/lib/go/thrift"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_comment_service/src/domain/comment/entity"
	"gitlab.com/bishe-projects/middle_comment_service/src/domain/comment/service"
)

var CommentApp = new(Comment)

type Comment struct{}

func (a *Comment) Publish(comment *entity.Comment) *business_error.BusinessError {
	return service.CommentDomain.Publish(comment)
}

func (a *Comment) Delete(comment *entity.Comment) *business_error.BusinessError {
	return service.CommentDomain.Delete(comment)
}

func (a *Comment) GetByID(id, bizId int64) (*entity.Comment, *business_error.BusinessError) {
	return service.CommentDomain.GetByID(id, bizId)
}

func (a *Comment) EntityCommentList(bizId, entityTypeId, entityId, parentId int64, pageNum, pageSize *int64) ([]*entity.Comment, int64, *business_error.BusinessError) {
	count, err := service.CommentDomain.GetListCount(bizId, entityTypeId, nil, thrift.Int64Ptr(entityId), thrift.Int64Ptr(parentId))
	if err != nil {
		return nil, 0, err
	}
	commentList, err := service.CommentDomain.GetList(bizId, entityTypeId, nil, thrift.Int64Ptr(entityId), thrift.Int64Ptr(parentId), pageNum, pageSize)
	return commentList, count, err
}

func (a *Comment) UserCommentList(bizId, entityTypeId, uid int64, pageNum, pageSize *int64) ([]*entity.Comment, int64, *business_error.BusinessError) {
	count, err := service.CommentDomain.GetListCount(bizId, entityTypeId, thrift.Int64Ptr(uid), nil, nil)
	if err != nil {
		return nil, 0, err
	}
	commentList, err := service.CommentDomain.GetList(bizId, entityTypeId, thrift.Int64Ptr(uid), nil, nil, pageNum, pageSize)
	return commentList, count, err
}
