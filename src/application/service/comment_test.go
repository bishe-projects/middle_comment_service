package service

import (
	"github.com/apache/thrift/lib/go/thrift"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"reflect"
	"testing"
)

func TestComment_UserCommentList(t *testing.T) {
	type args struct {
		bizId        int64
		entityTypeId int64
		uid          int64
		pageNum      *int64
		pageSize     *int64
	}
	tests := []struct {
		name    string
		args    args
		wantErr *business_error.BusinessError
	}{
		{
			name: "normal",
			args: args{
				bizId:        4,
				entityTypeId: 1,
				uid:          1,
				pageNum:      thrift.Int64Ptr(1),
				pageSize:     thrift.Int64Ptr(2),
			},
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &Comment{}
			commentList, count, err := a.UserCommentList(tt.args.bizId, tt.args.entityTypeId, tt.args.uid, tt.args.pageNum, tt.args.pageSize)
			if !reflect.DeepEqual(err, tt.wantErr) {
				t.Errorf("UserCommentList() got2 = %v, want %v", err, tt.wantErr)
			}
			t.Logf("count=%d", count)
			for idx, comment := range commentList {
				t.Logf("%d: %+v", idx, comment)
			}
		})
	}
}
