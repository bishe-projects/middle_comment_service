package service

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_comment_service/src/domain/comment_entity/entity"
	"gitlab.com/bishe-projects/middle_comment_service/src/domain/comment_entity/service"
)

var CommentEntityApp = new(CommentEntity)

type CommentEntity struct{}

func (a *CommentEntity) Create(commentEntity *entity.CommentEntity) *business_error.BusinessError {
	return service.CommentEntityDomain.Create(commentEntity)
}

func (a *CommentEntity) All(bizId *int64) ([]*entity.CommentEntity, *business_error.BusinessError) {
	return service.CommentEntityDomain.All(bizId)
}
