package entity

import (
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/common_utils/error_enum"
	"gitlab.com/bishe-projects/middle_comment_service/src/infrastructure/biz_error"
	"gitlab.com/bishe-projects/middle_comment_service/src/infrastructure/repo"
	"gitlab.com/bishe-projects/middle_comment_service/src/infrastructure/repo/po"
)

type CommentEntity struct {
	ID    int64
	BizId int64
	Name  string
	Desc  string
}

func (c *CommentEntity) Create() *business_error.BusinessError {
	commentEntityPO := c.ConvertToCommentEntityPO()
	err := repo.CommentEntityRepo.CommentEntityDao.Create(commentEntityPO)
	if err == error_enum.ErrDuplicateEntry {
		return business_error.DataExistsErr
	}
	if err != nil {
		klog.Errorf("[CommentEntityAggregate] create comment entity failed, err=%s", err)
		return biz_error.CreateCommentEntityErr
	}
	c.FillFromPO(commentEntityPO)
	return nil
}

type CommentEntityList []*CommentEntity

func (c *CommentEntityList) All(bizId *int64) *business_error.BusinessError {
	commentEntityPOList, err := repo.CommentEntityRepo.CommentEntityDao.All(bizId)
	if err != nil {
		klog.Errorf("[CommentEntityAggregate] get all comment entity failed, err=%s", err)
		return biz_error.AllCommentEntityListErr
	}
	c.FillFromPOList(commentEntityPOList)
	return nil
}

// converter
func (c *CommentEntity) ConvertToCommentEntityPO() *po.CommentEntity {
	return &po.CommentEntity{
		ID:    c.ID,
		BizId: c.BizId,
		Name:  c.Name,
		Desc:  c.Desc,
	}
}

func (c *CommentEntity) FillFromPO(commentEntityPO *po.CommentEntity) {
	c.ID = commentEntityPO.ID
	c.BizId = commentEntityPO.BizId
	c.Name = commentEntityPO.Name
	c.Desc = commentEntityPO.Desc
}

func (c *CommentEntityList) FillFromPOList(commentEntityPOList []*po.CommentEntity) {
	*c = make([]*CommentEntity, 0, len(commentEntityPOList))
	for _, commentEntityPO := range commentEntityPOList {
		commentEntity := new(CommentEntity)
		commentEntity.FillFromPO(commentEntityPO)
		*c = append(*c, commentEntity)
	}
}
