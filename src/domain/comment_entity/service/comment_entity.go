package service

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_comment_service/src/domain/comment_entity/entity"
)

var CommentEntityDomain = new(CommentEntity)

type CommentEntity struct{}

func (d *CommentEntity) Create(commentEntity *entity.CommentEntity) *business_error.BusinessError {
	return commentEntity.Create()
}

func (d *CommentEntity) All(bizId *int64) ([]*entity.CommentEntity, *business_error.BusinessError) {
	entityList := new(entity.CommentEntityList)
	err := entityList.All(bizId)
	return *entityList, err
}
