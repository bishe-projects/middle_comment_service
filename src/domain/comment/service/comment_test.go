package service

import (
	"github.com/apache/thrift/lib/go/thrift"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_comment_service/src/domain/comment/entity"
	"reflect"
	"testing"
)

func TestComment_Publish(t *testing.T) {
	type args struct {
		comment *entity.Comment
	}
	tests := []struct {
		name string
		args args
		want *business_error.BusinessError
	}{
		{
			name: "normal",
			args: args{
				comment: &entity.Comment{
					ID:           0,
					Uid:          2,
					BizId:        1,
					EntityTypeId: 1,
					EntityId:     2,
					Content:      "test2",
					ParentId:     0,
				},
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &Comment{}
			if got := d.Publish(tt.args.comment); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Publish() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestComment_Delete(t *testing.T) {
	type args struct {
		comment *entity.Comment
	}
	tests := []struct {
		name string
		args args
		want *business_error.BusinessError
	}{
		{
			name: "normal",
			args: args{
				comment: &entity.Comment{
					ID:    7,
					BizId: 1,
				},
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &Comment{}
			if got := d.Delete(tt.args.comment); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Delete() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestComment_GetList(t *testing.T) {
	type args struct {
		bizId        int64
		entityTypeId int64
		uid          *int64
		entityId     *int64
		parentId     *int64
		pageNum      *int64
		pageSize     *int64
	}
	tests := []struct {
		name  string
		args  args
		want  []*entity.Comment
		want1 *business_error.BusinessError
	}{
		{
			name: "entityCommentList",
			args: args{
				bizId:        1,
				entityTypeId: 1,
				entityId:     thrift.Int64Ptr(1),
				parentId:     thrift.Int64Ptr(0),
				pageNum:      thrift.Int64Ptr(1),
				pageSize:     thrift.Int64Ptr(1),
			},
		},
		{
			name: "userCommentList",
			args: args{
				bizId:        1,
				entityTypeId: 1,
				uid:          thrift.Int64Ptr(1),
				pageNum:      thrift.Int64Ptr(1),
				pageSize:     thrift.Int64Ptr(2),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &Comment{}
			got, got1 := d.GetList(tt.args.bizId, tt.args.entityTypeId, tt.args.uid, tt.args.entityId, tt.args.parentId, tt.args.pageNum, tt.args.pageSize)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetList() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("GetList() got1 = %v, want %v", got1, tt.want1)
			}
			for _, comment := range got {
				t.Logf("comment=%+v", comment)
			}
		})
	}
}

func TestComment_GetListCount(t *testing.T) {
	type args struct {
		bizId        int64
		entityTypeId int64
		uid          *int64
		entityId     *int64
		parentId     *int64
	}
	tests := []struct {
		name string
		args args
		want *business_error.BusinessError
	}{
		{
			name: "entityCommentList",
			args: args{
				bizId:        1,
				entityTypeId: 1,
				entityId:     thrift.Int64Ptr(1),
				parentId:     thrift.Int64Ptr(0),
			},
			want: nil,
		},
		{
			name: "userCommentList",
			args: args{
				bizId:        1,
				entityTypeId: 1,
				uid:          thrift.Int64Ptr(2),
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &Comment{}
			count, err := d.GetListCount(tt.args.bizId, tt.args.entityTypeId, tt.args.uid, tt.args.entityId, tt.args.parentId)
			t.Logf("count=%d", count)
			if !reflect.DeepEqual(err, tt.want) {
				t.Errorf("GetListCount() got1 = %v, want %v", err, tt.want)
			}
		})
	}
}
