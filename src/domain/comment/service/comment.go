package service

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_comment_service/src/domain/comment/entity"
)

var CommentDomain = new(Comment)

type Comment struct{}

func (d *Comment) Publish(comment *entity.Comment) *business_error.BusinessError {
	return comment.Publish()
}

func (d *Comment) Delete(comment *entity.Comment) *business_error.BusinessError {
	return comment.Delete()
}

func (d *Comment) GetByID(id, bizId int64) (*entity.Comment, *business_error.BusinessError) {
	comment := new(entity.Comment)
	err := comment.GetByID(id, bizId)
	if err == business_error.DataNotFoundErr {
		return nil, nil
	}
	return comment, err
}

func (d *Comment) GetListCount(bizId, entityTypeId int64, uid, entityId, parentId *int64) (int64, *business_error.BusinessError) {
	commentList := new(entity.CommentList)
	return commentList.Count(bizId, entityTypeId, uid, entityId, parentId)
}

func (d *Comment) GetList(bizId, entityTypeId int64, uid, entityId, parentId, pageNum, pageSize *int64) ([]*entity.Comment, *business_error.BusinessError) {
	commentList := new(entity.CommentList)
	err := commentList.Get(bizId, entityTypeId, uid, entityId, parentId, pageNum, pageSize)
	return *commentList, err
}
