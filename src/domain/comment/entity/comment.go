package entity

import (
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/common_utils/error_enum"
	"gitlab.com/bishe-projects/middle_comment_service/src/infrastructure/biz_error"
	"gitlab.com/bishe-projects/middle_comment_service/src/infrastructure/repo"
	"gitlab.com/bishe-projects/middle_comment_service/src/infrastructure/repo/po"
	"gorm.io/gorm"
	"time"
)

type Comment struct {
	ID           int64
	Uid          int64
	BizId        int64
	EntityTypeId int64
	EntityId     int64
	Content      string
	ParentId     int64
	CreateTime   time.Time
	UpdateTime   time.Time
	IsDelete     int8
	ReplyCount   int64
}

func (c *Comment) Publish() *business_error.BusinessError {
	commentPO := c.ConvertToPO()
	err := repo.CommentRepo.CommentDao.Create(commentPO)
	if err == error_enum.ErrDuplicateEntry {
		return business_error.DataExistsErr
	}
	if err != nil {
		klog.Errorf("[CommentAggregate] publish comment failed: err=%s", err)
		return biz_error.PublishCommentErr
	}
	c.FillFromPO(commentPO)
	return nil
}

func (c *Comment) Delete() *business_error.BusinessError {
	err := repo.CommentRepo.CommentDao.Delete(c.ID, c.BizId)
	if err != nil {
		klog.Errorf("[CommentAggregate] delete comment failed: err=%s", err)
		return biz_error.DeleteCommentErr
	}
	return nil
}

func (c *Comment) GetByID(id, bizId int64) *business_error.BusinessError {
	commentResultPO, err := repo.CommentRepo.CommentDao.GetByID(id, bizId)
	if err == gorm.ErrRecordNotFound {
		return business_error.DataNotFoundErr
	}
	if err != nil {
		klog.Errorf("[CommentAggregate] get comment by id failed: err=%s", err)
		return biz_error.GetCommentByIDErr
	}
	c.FillFromResultPO(commentResultPO)
	return nil
}

type CommentList []*Comment

func (c *CommentList) Count(bizId, entityTypeId int64, uid, entityId, parentId *int64) (int64, *business_error.BusinessError) {
	count, err := repo.CommentRepo.CommentDao.Count(bizId, entityTypeId, uid, entityId, parentId)
	if err != nil {
		klog.Errorf("[CommentAggregate] get count failed: err=%s", err)
		return 0, biz_error.GetCommentListCountErr
	}
	return count, nil
}

func (c *CommentList) Get(bizId, entityTypeId int64, uid, entityId, parentId, pageNum, pageSize *int64) *business_error.BusinessError {
	commentPOList, err := repo.CommentRepo.CommentDao.GetList(bizId, entityTypeId, uid, entityId, parentId, pageNum, pageSize)
	if err != nil {
		klog.Errorf("[CommentAggregate] get list failed: err=%s", err)
		return biz_error.GetCommentListErr
	}
	c.FillFromPOList(commentPOList)
	return nil
}

// converter
func (c *Comment) ConvertToPO() *po.Comment {
	return &po.Comment{
		ID:           c.ID,
		Uid:          c.Uid,
		BizId:        c.BizId,
		EntityTypeId: c.EntityTypeId,
		EntityId:     c.EntityId,
		Content:      c.Content,
		ParentId:     c.ParentId,
		CreateTime:   c.CreateTime,
		UpdateTime:   c.UpdateTime,
		IsDelete:     c.IsDelete,
	}
}

func (c *Comment) FillFromPO(commentPO *po.Comment) {
	c.ID = commentPO.ID
	c.Uid = commentPO.Uid
	c.BizId = commentPO.BizId
	c.EntityTypeId = commentPO.EntityTypeId
	c.EntityId = commentPO.EntityId
	c.Content = commentPO.Content
	c.ParentId = commentPO.ParentId
	c.CreateTime = commentPO.CreateTime
	c.UpdateTime = commentPO.UpdateTime
	c.IsDelete = commentPO.IsDelete
}

func (c *Comment) FillFromResultPO(resultPO *po.CommentResult) {
	c.ID = resultPO.ID
	c.Uid = resultPO.Uid
	c.BizId = resultPO.BizId
	c.EntityTypeId = resultPO.EntityTypeId
	c.EntityId = resultPO.EntityId
	c.Content = resultPO.Content
	c.ParentId = resultPO.ParentId
	c.CreateTime = resultPO.CreateTime
	c.ReplyCount = resultPO.ReplyCount
}

func (c *CommentList) FillFromPOList(commentPOList []*po.CommentResult) {
	*c = make([]*Comment, 0, len(commentPOList))
	for _, commentPO := range commentPOList {
		comment := new(Comment)
		comment.FillFromResultPO(commentPO)
		*c = append(*c, comment)
	}
}
